# What is this?

A quick demo I made for Xanda and my Feb. 4th 2016 CS6784 presentation. It is an implementation of some experiments from


```
@inproceedings{glorot2010understanding,
  title={Understanding the difficulty of training deep feedforward neural networks},
  author={Glorot, Xavier and Bengio, Yoshua},
  booktitle={International conference on artificial intelligence and statistics},
  pages={249--256},
  year={2010}
}
```
## Requirements
[Keras](http://keras.io/), and either [Tensorflow](https://www.tensorflow.org/) or [Theano](http://deeplearning.net/software/theano/) (it runs in both, thanks to Keras' abstracted backend module!)